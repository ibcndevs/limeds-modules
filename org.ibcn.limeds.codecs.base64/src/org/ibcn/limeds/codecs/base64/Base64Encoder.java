package org.ibcn.limeds.codecs.base64;

import java.nio.charset.Charset;
import java.util.Base64;
import java.util.Base64.Encoder;

import org.ibcn.limeds.FunctionalSegment;
import org.ibcn.limeds.annotations.InputDoc;
import org.ibcn.limeds.annotations.OutputDoc;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Validated;
import org.ibcn.limeds.json.JsonPrimitive;
import org.ibcn.limeds.json.JsonValue;

@Segment(id = "limeds.codecs.Base64Encoder")
@Validated(input = true, output = false)
public class Base64Encoder implements FunctionalSegment {

	@Override
	@InputDoc(label = "text", schema = "\"String (String to encode)\"")
	@InputDoc(label = "type", schema = "\"String * [basic, mime, url] (Base64 encoding scheme, defaults to basic)\"")
	@OutputDoc(schema = "\"String\"")
	public JsonValue apply(JsonValue... input) throws Exception {
		Encoder encoder = Base64.getEncoder();
		if (input.length > 1) {
			switch (input[1].asString()) {
			case "mime":
				encoder = Base64.getMimeEncoder();
				break;
			case "url":
				encoder = Base64.getUrlEncoder();
				break;
			case "basic":
			default:
				encoder = Base64.getEncoder();
			}
		}

		return new JsonPrimitive(new String(encoder.encode(input[0].asString().getBytes()), Charset.forName("UTF-8")));
	}

}
