/**
 * This package specifies and implements the LimeDS JSON model.
 */
@org.osgi.annotation.versioning.Version("2.0.0")
package org.ibcn.limeds.codecs.base64;
