package org.ibcn.limeds.codecs.base64;

import java.nio.charset.Charset;
import java.util.Base64;
import java.util.Base64.Decoder;

import org.ibcn.limeds.FunctionalSegment;
import org.ibcn.limeds.annotations.InputDoc;
import org.ibcn.limeds.annotations.OutputDoc;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Validated;
import org.ibcn.limeds.json.JsonPrimitive;
import org.ibcn.limeds.json.JsonValue;

@Segment(id = "limeds.codecs.Base64Decoder")
@Validated(input = true, output = false)
public class Base64Decoder implements FunctionalSegment {

	@Override
	@InputDoc(label = "text", schema = "\"String (String representation of Base64 encoded text)\"")
	@InputDoc(label = "type", schema = "\"String * [basic, mime, url] (Base64 decoding scheme, defaults to basic)\"")
	@OutputDoc(schema = "\"String\"")
	public JsonValue apply(JsonValue... input) throws Exception {
		Decoder decoder = Base64.getDecoder();
		if (input.length > 1) {
			switch (input[1].asString()) {
			case "mime":
				decoder = Base64.getMimeDecoder();
				break;
			case "url":
				decoder = Base64.getUrlDecoder();
				break;
			case "basic":
			default:
				decoder = Base64.getDecoder();
			}
		}

		return new JsonPrimitive(new String(decoder.decode(input[0].asString()), Charset.forName("UTF-8")));
	}

}
