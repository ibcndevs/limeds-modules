package org.ibcn.limeds.ifttt;

import org.ibcn.commons.restclient.api.Client;
import org.ibcn.limeds.FunctionalSegment;
import org.ibcn.limeds.annotations.Configurable;
import org.ibcn.limeds.annotations.InputDoc;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonObjectBuilder;
import org.ibcn.limeds.json.JsonPrimitive;
import org.ibcn.limeds.json.JsonValue;

@Segment(factory = true, description = "Send custom events to the IFTTT Maker channel. With an account on IFTTT, use the Maker channel to get your secret (https://ifttt.com/maker)")
public class MakerEventSender implements FunctionalSegment {
	@Configurable(description = "Your secret key, find it at https://internal-api.ifttt.com/maker when logged in.")
	private String secretKey;

	@Configurable(description = "The name of the event. Can be used in IFTTT to listen for as trigger.", defaultValue = "my_event")
	private String eventName;

	@Service
	private Client client;

	final private String urlPart1 = "https://maker.ifttt.com/trigger/";
	final private String urlPart2 = "/with/key/";

	@Override
	@InputDoc(label = "value1", schema = "{\"@typeInfo\" : \"Object * (Any JSON object you want, you can also use a String)\"}")
	@InputDoc(label = "value2", schema = "{\"@typeInfo\" : \"Object * (Any JSON object you want, you can also use a String)\"}")
	@InputDoc(label = "value3", schema = "{\"@typeInfo\" : \"Object * (Any JSON object you want, you can also use a String)\"}")
	public JsonValue apply(JsonValue... input) throws Exception {
		String url = urlPart1.concat(eventName).concat(urlPart2).concat(secretKey);
		JsonObjectBuilder builder = Json.objectBuilder();
		int size = input.length;
		for (int i = 0; i < Math.min(3, size); i++) {
			builder.add("value" + (i + 1), input[i]);
		}
		return new JsonPrimitive(client.target(url).postJson(builder.build().toString()).returnString().getBody());
	}

}
