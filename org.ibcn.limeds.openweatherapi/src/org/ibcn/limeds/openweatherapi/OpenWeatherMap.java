package org.ibcn.limeds.openweatherapi;

import org.ibcn.commons.restclient.api.Client;
import org.ibcn.limeds.ServiceSegment;
import org.ibcn.limeds.annotations.Configurable;
import org.ibcn.limeds.annotations.InputDoc;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.annotations.ServiceOperation;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonValue;
import org.ibcn.limeds.openweatherapi.api.WeatherApi;

/**
 * Produces basic weather info for location, city, zipcode
 * 
 * @author tdupont
 *
 */

@Segment(id = "org.ibcn.limeds.openweathermap", description = "OpenWeatherMap service for weather related info.")
public class OpenWeatherMap extends ServiceSegment implements WeatherApi {
	public static final String LIBRARY_VERSION = "2.0.0";
	private static final String ENDPOINT = "http://api.openweathermap.org/data/2.5/weather";

	@Configurable(defaultValue = "", description = "The API KEY for using the OpenWeatherMap API.")
	private String APP_ID = "";

	@Service
	private Client httpClient;

	@Override
	@ServiceOperation(description = "Returns the current weather, string matching the given city.")
	public JsonValue weatherByCity(String city) throws Exception {
		return Json.from(httpClient.target(ENDPOINT)
				.withQuery("APPID", APP_ID)
				.withQuery("q", city).get()
				.returnString().getBody());
	}

	@Override
	@ServiceOperation(description = "Returns the current weather, string matching the given city an country code.")
	@InputDoc(label = "city", schema = "\"String (The city)\"")
	@InputDoc(label = "countryCode", schema = "\"String (Use the ISO 3166 country codes)\"")
	public JsonValue weatherByCity(String city, String countryCode) throws Exception {
		return Json.from(httpClient.target(ENDPOINT)
				.withQuery("APPID", APP_ID)
				.withQuery("q", city+","+countryCode).get()
				.returnString().getBody());
	}

	@Override
	@ServiceOperation(description = "Returns the current weather, using the given city id. Find it at http://bulk.openweathermap.org/sample/")
	@InputDoc(label = "cityId", schema = "\"String (The unique cityId)\"")
	public JsonValue weatherByCityId(String cityId) throws Exception {
		return Json.from(httpClient.target(ENDPOINT)
				.withQuery("APPID", APP_ID)
				.withQuery("id", cityId).get()
				.returnString().getBody());
	}

	@Override
	@ServiceOperation(description = "Returns the current weather, using the given geolocation.")
	@InputDoc(label = "latitude", schema = "\"FloatNumber (latitude)\"")
	@InputDoc(label = "longitude", schema = "\"FloatNumber (longitude)\"")
	public JsonValue weatherByLocation(double latitude, double longitude) throws Exception {
		return Json.from(httpClient.target(ENDPOINT)
				.withQuery("APPID", APP_ID)
				.withQuery("lat", latitude+"")
				.withQuery("lon", longitude+"").get()
				.returnString().getBody());
	}

	@Override
	@ServiceOperation(description = "Returns the current weather, using the given zip code and country code.")
	@InputDoc(label = "zipcode", schema = "\"IntNumber (Postal/zip code)\"")
	@InputDoc(label = "countryCode", schema = "\"String (2 letter country code, eg. be, uk, us)\"")
	public JsonValue weatherByZipcode(int zipcode, String countryCode) throws Exception {
		return Json.from(httpClient.target(ENDPOINT)
				.withQuery("APPID", APP_ID)
				.withQuery("zip", zipcode+","+countryCode).get()
				.returnString().getBody());
	}

}
