package org.ibcn.limeds.openweatherapi.api;

import org.ibcn.limeds.json.JsonValue;

public interface WeatherApi {

	/**
	 * Returns the current weather, given a certain city.
	 * @param city The city in question
	 * @return
	 * @throws Exception
	 */
	JsonValue weatherByCity(String city) throws Exception;
	
	/**
	 * Returns the current weather, given a certain city and country.
	 * @param city The city in question
	 * @param countryCode Use the ISO 3166 country codes
	 * @return
	 * @throws Exception
	 */
	JsonValue weatherByCity(String city, String countryCode) throws Exception;
	
	/**
	 * Returns the current weather, given a certain city id.
	 * @param cityId {@link http://bulk.openweathermap.org/sample/}
	 * @return
	 * @throws Exception
	 */
	JsonValue weatherByCityId(String cityId) throws Exception;
	
	/**
	 * Returns the current weather, given a certain geolocation.
	 * @param latitude
	 * @param longitude
	 * @return
	 * @throws Exception
	 */
	JsonValue weatherByLocation(double latitude, double longitude) throws Exception;
	
	/**
	 Returns the current weather, given a certain zipcode.
	 * @param zipcode Postal code of city
	 * @param countryCode Use the ISO 3166 country codes
	 * @return
	 * @throws Exception
	 */
	JsonValue weatherByZipcode(int zipcode, String countryCode) throws Exception;
}
