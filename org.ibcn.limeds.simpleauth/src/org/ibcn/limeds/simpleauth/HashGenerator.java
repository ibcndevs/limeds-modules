package org.ibcn.limeds.simpleauth;

import java.security.MessageDigest;

import org.ibcn.limeds.HttpEndpointSegment;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.HttpOperation;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonValue;

@Segment
public class HashGenerator extends HttpEndpointSegment {

	@Override
	@HttpOperation(groupId = "Auth APIs", method = HttpMethod.GET, path = "/auth/hash/{password}")
	public JsonValue apply(JsonValue input, HttpContext context) throws Exception {
		return Json.objectBuilder().add("passwordHash", hashString(context.pathParameters().getString("password")))
				.build();
	}

	public static String hashString(String target) throws Exception {
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		md.update(target.getBytes());

		byte byteData[] = md.digest();

		// convert the byte to hex format
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < byteData.length; i++) {
			sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
		}

		return sb.toString();
	}

}
