package org.ibcn.limeds.simpleauth;

import java.io.IOException;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ibcn.limeds.HttpEndpointSegment;
import org.ibcn.limeds.annotations.Configurable;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.HttpOperation;
import org.ibcn.limeds.annotations.InputDoc;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Validated;
import org.ibcn.limeds.descriptors.HttpOperationDescriptor;
import org.ibcn.limeds.exceptions.ExceptionWithStatus;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonValue;
import org.ibcn.limeds.security.HttpAuthProvider;
import org.ibcn.limeds.security.SystemRoles;
import org.ibcn.limeds.util.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Segment
@Validated
public class Provider extends HttpEndpointSegment implements HttpAuthProvider {

	private static final String TOKEN_ATTRIBUTE = "token";
	private static final Logger LOGGER = LoggerFactory.getLogger(Provider.class);

	@Configurable(defaultValue = "false")
	private boolean hashPasswords;

	/*
	 * Array of valid accounts formatted as:
	 * [username];[password];[roleA];[roleB];...
	 */
	@Configurable(defaultValue = "admin;admin;" + SystemRoles.LIMEDS_ADMIN)
	private String[] accounts;

	private volatile int accountsHash = Integer.MIN_VALUE;

	private Map<String, JsonObject> activeTokens = new HashMap<>();

	@Override
	public Optional<JsonValue> authorizeRequest(HttpServletRequest request, HttpServletResponse response,
			HttpOperationDescriptor httpOperation) {
		validateTokenStore();

		// Parse the token from the HTTP request
		Optional<String> token = parseToken(request);

		// Look up this token in memory
		if (token.isPresent() && activeTokens.containsKey(token.get())
				&& rolesMatch(activeTokens.get(token.get()).get("roles").asArray().stream()
						.map(jsonRole -> jsonRole.asString()).collect(Collectors.toSet()), httpOperation)) {
			/*
			 * If present, return authentication information, indicating
			 * success.
			 */
			return Optional.of(Json.objectBuilder().add("userid", activeTokens.get(token.get())).build());
		} else {
			// If not, write an error HTTP response...
			response.setHeader("WWW-Authenticate", "session-cookie: " + TOKEN_ATTRIBUTE);
			try {
				response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
						"Valid credentials are required to access this operation!");
			} catch (IOException e) {
				e.printStackTrace();
			}
			// ... and return an empty optional, indicating failure.
			return Optional.empty();
		}
	}

	/**
	 * Simple check functions to prevent users logging in with an old token
	 * after the password was changed.
	 */
	private void validateTokenStore() {
		if (accountsHash == Integer.MIN_VALUE) {
			accountsHash = accounts.hashCode();
		}

		if (accountsHash != accounts.hashCode()) {
			activeTokens.clear();
			accountsHash = accounts.hashCode();
		}
	}

	@Override
	@InputDoc(schema = "{\"username\" : \"String\", \"password\" : \"String\"}")
	@HttpOperation(groupId = "Auth APIs", method = HttpMethod.POST, path = "/auth/tokens")
	public JsonValue apply(JsonValue input, HttpContext context) throws Exception {
		Map<String, Pair<String, List<String>>> accountInfo = getAccountInfo();
		String username = input.getString("username");
		String password = input.getString("password");
		if (accountInfo.containsKey(username) && passwordMatch(password, accountInfo.get(username).getKey())) {
			String token = Base64.getUrlEncoder().encodeToString(UUID.randomUUID().toString().getBytes());
			activeTokens.put(token, Json.objectBuilder().add("userid", username)
					.add("roles", Json.arrayOf(accountInfo.get(username).getValue().toArray(new String[] {}))).build());
			context.respondWith("Set-Cookie", TOKEN_ATTRIBUTE + "=" + token + ";Path=/; HttpOnly");
			return null;
		} else {
			throw new ExceptionWithStatus(401);
		}
	}

	private Map<String, Pair<String, List<String>>> getAccountInfo() {
		try {
			Map<String, Pair<String, List<String>>> info = new HashMap<>();
			for (String entry : accounts) {
				List<String> record = Arrays.stream(entry.split(";")).map(String::trim).collect(Collectors.toList());
				info.put(record.get(0),
						new Pair<String, List<String>>(record.get(1), record.subList(2, record.size())));
			}
			return info;
		} catch (Exception e) {
			LOGGER.warn("The configuration for the Simple Auth Service is not correctly formatted!");
			return new HashMap<>();
		}
	}

	private Optional<String> parseToken(HttpServletRequest request) {
		if (request.getHeader("Cookie") != null) {
			return Arrays.stream(request.getHeader("Cookie").split(";")).map(s -> s.trim())
					.filter(s -> s.startsWith(TOKEN_ATTRIBUTE)).map(s -> s.split("=")[1].trim()).findAny();
		}
		return Optional.empty();
	}

	private boolean passwordMatch(String input, String password) throws Exception {
		if (hashPasswords) {
			return password.equals(HashGenerator.hashString(input));
		} else {
			return password.equals(input);
		}
	}

	private boolean rolesMatch(Set<String> roles, HttpOperationDescriptor httpOperation) {
		Set<String> requiredRoles = Arrays.stream(httpOperation.getAuthorityRequired()).collect(Collectors.toSet());
		switch (httpOperation.getAuthMode()) {
		case AUTHORIZE_CONJUNCTIVE:
			return requiredRoles.stream().allMatch(role -> roles.contains(role));
		case AUTHORIZE_DISJUNCTIVE:
			return requiredRoles.stream().anyMatch(role -> roles.contains(role));
		default:
			return true;
		}
	}

}
