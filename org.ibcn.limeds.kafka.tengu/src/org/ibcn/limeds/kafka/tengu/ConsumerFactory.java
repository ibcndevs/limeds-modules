package org.ibcn.limeds.kafka.tengu;

import java.io.IOException;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ibcn.limeds.EventBus;
import org.ibcn.limeds.FunctionalSegment;
import org.ibcn.limeds.annotations.Configurable;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.httpclient.api.HttpClient;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonValue;
import org.osgi.service.http.HttpService;
import org.slf4j.LoggerFactory;

@SuppressWarnings("serial")
@Segment(factory = true)
public class ConsumerFactory extends HttpServlet implements FunctionalSegment {

	@Service
	private HttpService httpService;

	@Service
	private EventBus eventBus;

	@Service
	private HttpClient httpClient;

	@Configurable
	private String subscriberEndpoint;

	@Configurable
	private String callbackEndpoint;

	@Configurable
	private String[] topics;

	@Configurable
	private String channelPrefix;

	private String path;

	public synchronized void started() throws Exception {
		// Set up callback endpoint
		try {
			path = new URL(callbackEndpoint).getPath();

			httpService.registerServlet(path, this, null, null);
		} catch (Exception e) {
			throw new Exception("Could not set up the Kafka callback endpoint.");
		}

		// Register callback with Tengu Kafka service
		try {
			httpClient.put(
					subscriberEndpoint.endsWith("/") ? subscriberEndpoint + "subscribe"
							: subscriberEndpoint + "/subscribe",
					Json.objectBuilder().add("topics", Json.arrayOf(topics)).add("endpoint", callbackEndpoint).build());
		} catch (Exception e) {
			throw new Exception("Could not register the Kafka callback endpoint with " + subscriberEndpoint);
		}
	}

	public synchronized void stopped() {
		// Unregister callback with Tengu Kafka service
		try {
			httpClient.doRequest(Json.objectBuilder().add("method", "DELETE")
					.add("targetUrl",
							subscriberEndpoint.endsWith("/") ? subscriberEndpoint + "unsubscribe"
									: subscriberEndpoint + "/unsubscribe")
					.add("body", Json.objectBuilder().add("endpoint", callbackEndpoint)).build());
		} catch (Exception e) {
			LoggerFactory.getLogger(getClass()).warn(
					"Error while trying to unregister the callback with the Kafka service at " + subscriberEndpoint);
		}

		// Unregister callback endpoint (not applicable for now)
		/*
		 * try { if (path != null) { httpService.unregister(path); } } catch
		 * (Exception e) { LoggerFactory.getLogger(getClass())
		 * .warn("Error while trying to unregister the Kafka callback endpoint. "
		 * + subscriberEndpoint); }
		 */
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		JsonObject message = null;
		try {
			message = Json.from(req.getInputStream());
		} catch (Exception e) {
			resp.sendError(400, "Could not parse the Kafka message!");
			return;
		}

		if (message != null && message.has("topic")) {
			eventBus.broadcast(channelPrefix + message.getString("topic"), message);
		} else {
			resp.sendError(400, "The message must specify a topic!");
		}
	}

	@Override
	public JsonValue apply(JsonValue... input) throws Exception {
		return null;
	}

}
