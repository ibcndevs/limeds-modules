@org.osgi.annotation.versioning.Version(org.ibcn.limeds.influxdb.InfluxDriver.INFLUX_MODULE_VERSION)
@org.ibcn.limeds.annotations.IncludeTypes("ResultSet.json")
package org.ibcn.limeds.influxdb;