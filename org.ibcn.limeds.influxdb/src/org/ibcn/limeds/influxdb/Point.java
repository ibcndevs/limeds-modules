package org.ibcn.limeds.influxdb;

import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.ibcn.limeds.annotations.JsonAttribute;
import org.ibcn.limeds.annotations.JsonType;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonPrimitive;
import org.ibcn.limeds.json.JsonValue;

@JsonType
public class Point {

	@JsonAttribute(optional = true)
	private String measurement;

	@JsonAttribute(optional = true)
	private Long timestampValue = null;
	@JsonAttribute(optional = true)
	private TimeUnit timestampUnit = TimeUnit.MILLISECONDS;

	@JsonAttribute(optional = true)
	private JsonObject tags;
	private JsonObject fields;

	public Point() {
		this("", new JsonObject(), new JsonObject());
	}

	public Point(String measurement, JsonObject fields) {
		this(measurement, new JsonObject(), fields);
	}

	public Point(String measurement, JsonObject fields, Long timestampValue, TimeUnit timestampUnit) {
		this(measurement, new JsonObject(), fields, timestampValue, timestampUnit);
	}

	public Point(String measurement, JsonObject tags, JsonObject fields) {
		this(measurement, tags, fields, null, TimeUnit.MILLISECONDS);
	}

	public Point(String measurement, JsonObject tags, JsonObject fields, Long timestampValue, TimeUnit timestampUnit) {
		this.measurement = measurement;
		this.timestampValue = timestampValue;
		this.timestampUnit = timestampUnit;
		this.tags = tags;
		this.fields = fields;
	}

	public String getMeasurement() {
		return measurement;
	}

	public void setMeasurement(String measurement) {
		this.measurement = measurement;
	}

	public long getTimestampValue() {
		return timestampValue;
	}

	public void setTimestampValue(long timestampValue) {
		this.timestampValue = timestampValue;
	}

	public TimeUnit getTimestampUnit() {
		return timestampUnit;
	}

	public void setTimestampUnit(TimeUnit timestampUnit) {
		this.timestampUnit = timestampUnit;
	}

	public JsonObject getTags() {
		return tags;
	}

	public void setTags(JsonObject tags) {
		this.tags = tags;
	}

	public JsonObject getFields() {
		return fields;
	}

	public void setFields(JsonObject fields) {
		this.fields = fields;
	}

	@Override
	public String toString() {
		String tagsPart = tags.size() > 0 ? "," + tags.entrySet().stream()
				.map(e -> e.getKey() + "=" + e.getValue().asString()).collect(Collectors.joining(",")) : "";
		String timePart = timestampValue != null ? " " + TimeUnit.NANOSECONDS.convert(timestampValue, timestampUnit)
				: "";
		return measurement + tagsPart + " " + fields.entrySet().stream()
				.map(e -> e.getKey() + "=" + jsonValToString(e.getValue())).collect(Collectors.joining(",")) + timePart;
	}

	private String jsonValToString(JsonValue val) {
		return (val instanceof JsonPrimitive && ((JsonPrimitive) val).getValue() instanceof String)
				? "\"" + val.asString() + "\"" : val.toString();
	}

}
