package org.ibcn.limeds.influxdb;

import org.ibcn.commons.restclient.api.Client;
import org.ibcn.limeds.ServiceSegment;
import org.ibcn.limeds.annotations.Configurable;
import org.ibcn.limeds.annotations.InputDoc;
import org.ibcn.limeds.annotations.OutputDoc;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.annotations.ServiceOperation;
import org.ibcn.limeds.influxdb.api.InfluxDB;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonValue;
import org.ibcn.limeds.util.Errors;

@Segment(factory = true, id = "org.ibcn.limeds.db.InfluxDB", description = "InfluxDB connector used to talk with an InfluxDB host.")
public class DefaultInfluxDB extends ServiceSegment implements InfluxDB {

	@Configurable(defaultValue = "localhost", description = "The host InfluxDB is running on.")
	private String host;

	@Configurable(defaultValue = "8086", description = "The port InfluxDB is running on.")
	private int port;

	@Configurable(description = "The InfluxDB database to connect with.")
	private String database;

	@Service
	private Client restClient;

	private InfluxDriver storageDriver;

	@Override
	public void started() throws Exception {
		System.out.println("Influx started with " + host + ":" + port + "/" + database);
		storageDriver = new InfluxDriver(host, port, database, restClient);
	}

	@Override
	public void stopped() throws Exception {
		System.out.println("Influx stopped");
		storageDriver = null;
	}

	@Override
	@ServiceOperation(description = "Store a json object in a measurement series.")
	@InputDoc(label = "measurementName", schema = "\"String (Name of the measurement series)\"")
	@InputDoc(label = "object", schema = "{\"@typeInfo\": \"Object (Json object to store)\"}")
	public JsonValue store(String measurement, JsonValue object) throws Exception {
		if (object instanceof JsonObject) {
			object.asObject().put("measurement", measurement);
			storageDriver.write(Json.to(Point.class, object.asObject()));
		} else if (object instanceof JsonArray) {
			Point[] points = object.asArray().stream().map(Errors.wrapFunction(p -> {
				p.asObject().put("measurement", measurement);
				return Json.to(Point.class, p.asObject());
			})).toArray(size -> new Point[size]);
			storageDriver.write(points);
		}
		return null;
	}

	@Override
	@ServiceOperation(description = "Converts an InfluxDB time value string to a unix timestamp (millis)")
	@InputDoc(label = "influxTimeValue", schema = "\"String (InfluxDB time value string)\"")
	@OutputDoc(schema = "\"IntNumber (Unix epoch timestamp (in millis))\"")
	public long timeToMillis(String influxTimeValue) throws Exception {
		return storageDriver.timeToMillis(influxTimeValue);
	}

	@Override
	@ServiceOperation(description = "Performs a single query on the database and returns a ResultSet in JSON.")
	@InputDoc(label = "queryExpression", schema = "\"String (The query expression as a String.)\"")
	@OutputDoc(schemaRef = "org.ibcn.limeds.influxdb.ResultSet_${sliceVersion}")
	public JsonValue query(String queryExpression) throws Exception {
		return storageDriver.queryNative(queryExpression);
	}

	@Override
	@ServiceOperation(description = "Performs multiple queries on the database and returns a single ResultSet in JSON.")
	@InputDoc(label = "queryExpressions", schema = "\"String (The query expression as a String.)\"", collection = true)
	@OutputDoc(schemaRef = "org.ibcn.limeds.influxdb.ResultSet_${sliceVersion}")
	public JsonValue multiQuery(JsonArray queryExpressions) throws Exception {
		return storageDriver
				.queryNative(queryExpressions.stream().map(e -> e.asString()).toArray(size -> new String[size]));
	}

}
