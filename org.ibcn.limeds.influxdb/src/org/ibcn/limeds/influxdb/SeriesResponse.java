package org.ibcn.limeds.influxdb;

import java.util.List;

import org.ibcn.limeds.annotations.JsonAttribute;
import org.ibcn.limeds.annotations.JsonType;
import org.ibcn.limeds.json.JsonArray;

@JsonType
public class SeriesResponse {

	@JsonAttribute(desc = "The name of the series.")
	private String name;
	@JsonAttribute(desc = "In order list of the columns (by id) present in the collected values.")
	private List<String> columns;
	@JsonAttribute(desc = "List of the retrieved values.")
	private List<JsonArray> values;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<String> getColumns() {
		return columns;
	}
	public void setColumns(List<String> columns) {
		this.columns = columns;
	}
	public List<JsonArray> getValues() {
		return values;
	}
	public void setValues(List<JsonArray> values) {
		this.values = values;
	}

}
