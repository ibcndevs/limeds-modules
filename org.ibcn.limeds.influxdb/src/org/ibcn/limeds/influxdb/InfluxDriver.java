package org.ibcn.limeds.influxdb;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.TimeZone;
import java.util.stream.Collectors;

import org.ibcn.commons.restclient.api.Client;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonObject;
import org.osgi.service.component.annotations.Reference;

public class InfluxDriver {

	public static final String INFLUX_MODULE_VERSION = "3.0.0";

	@Reference
	private transient Client restClient;
	private String host;
	private int port;
	private String database;

	public InfluxDriver(String host, int port, String database, Client restClient) {
		this.host = host;
		this.port = port;
		this.database = database;
		this.restClient = restClient;
	}

	public void write(Point... points) throws Exception {
		String lines = Arrays.stream(points).map(Point::toString).collect(Collectors.joining("\n"));
		restClient.target(getURL("write")).withQuery("db", database).post(lines).returnNoResult();
	}

	public JsonObject queryNative(String... queryExpr) throws Exception {
		return restClient.target(getURL("query")).withQuery("db", database)
				.withQuery("q", Arrays.stream(queryExpr).collect(Collectors.joining(";"))).withQuery("epoch", "ms")
				.get().returnObject(Json.getDeserializer()).getBody().asObject();
	}

	private String getURL(String action) {
		return "http://" + host + ":" + port + "/" + action;
	}

	public long timeToMillis(String influxTimeValue) throws Exception {
		try {
			String[] parts = influxTimeValue.split("T");
			String datePart = parts[0];
			String timePart = parts[1].substring(0, parts[1].length() - 1);
			SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat time = new SimpleDateFormat("HH:mm:ss.SSS");
			date.setTimeZone(TimeZone.getTimeZone("UTC"));
			time.setTimeZone(TimeZone.getTimeZone("UTC"));

			return date.parse(datePart).getTime() + time.parse(timePart).getTime();
		} catch (Exception e) {
			throw new Exception("Could not parse date format", e);
		}
	}

}
