package org.ibcn.limeds.influxdb.api;

import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonValue;

public interface InfluxDB {

	/**
	 * Store a json object in a measurement series.
	 * 
	 * @param measurement
	 *            The id of the measurement series.
	 * @param object
	 *            Json to store
	 * @return
	 */
	JsonValue store(String measurement, JsonValue object) throws Exception;

	/**
	 * Query the database with a query expression.
	 * 
	 * @param queryExpression
	 *            The query expression to use.
	 * @return A json array with the results
	 */
	JsonValue query(String queryExpression) throws Exception;

	/**
	 * Query the database with multiple query expression in a single request.
	 * 
	 * @param queryExpressions
	 *            A json array containing Strings representing the queries.
	 * @return A json array with the results
	 */
	JsonValue multiQuery(JsonArray queryExpressions) throws Exception;

	/**
	 * Converts an influx time value to a unix timestamp in milliseconds
	 * 
	 * @param influxTimeValue
	 *            The influx time value
	 * @return A unix timestamp in milliseconds
	 */
	long timeToMillis(String influxTimeValue) throws Exception;
}
