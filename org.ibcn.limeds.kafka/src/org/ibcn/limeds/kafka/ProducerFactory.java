package org.ibcn.limeds.kafka;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.ibcn.limeds.FunctionalSegment;
import org.ibcn.limeds.annotations.Configurable;
import org.ibcn.limeds.annotations.InputDoc;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.json.JsonValue;

@Segment(factory = true)
public class ProducerFactory implements FunctionalSegment {

	@Configurable(defaultValue = "localhost:9092")
	private String[] bootstrapServers;

	@Configurable(defaultValue = "")
	private String clientId;

	@Configurable(defaultValue = "1")
	private String acks;

	private Producer<String, String> kafkaProducer;

	@Override
	public void started() {
		Properties props = new Properties();
		props.put("bootstrap.servers", bootstrapServers.length > 1 ? bootstrapServers : bootstrapServers[0]);
		props.put("acks", acks);
		if (!clientId.isEmpty()) {
			props.put("client.id", clientId);
		}
		kafkaProducer = new KafkaProducer<>(props);
	}

	@Override
	public void stopped() throws Exception {
		kafkaProducer.close();
		kafkaProducer = null;
	}

	@InputDoc(label = "topic", schema = "\"String (The kafka topic to post the message to.)\"")
	@InputDoc(label = "message", schema = "{}")
	@Override
	public JsonValue apply(JsonValue... input) throws Exception {
		kafkaProducer.send(new ProducerRecord<String, String>(input[0].asString(), input[1].toString()));
		return null;
	}

}
