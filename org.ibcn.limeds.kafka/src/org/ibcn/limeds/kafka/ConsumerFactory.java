package org.ibcn.limeds.kafka;

import java.util.Arrays;
import java.util.Properties;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;
import org.ibcn.limeds.EventBus;
import org.ibcn.limeds.FunctionalSegment;
import org.ibcn.limeds.annotations.Configurable;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonValue;
import org.slf4j.LoggerFactory;

@Segment(factory = true, export = false)
public class ConsumerFactory implements FunctionalSegment, Runnable {

	private static final String KAFKA_EVENT_PREFIX = "kafka.";

	@Configurable(defaultValue = "localhost:9092")
	private String[] bootstrapServers;

	// The consumer group id
	@Configurable
	private String groupId;

	@Configurable
	private String[] topics;

	@Service
	private EventBus eventBus;

	private Consumer<String, String> kafkaConsumer;

	@Override
	public void started() throws Exception {
		Properties props = new Properties();
		props.put("bootstrap.servers", bootstrapServers.length > 1 ? bootstrapServers : bootstrapServers[0]);
		props.put("group.id", groupId);
		kafkaConsumer = new KafkaConsumer<>(props);

		// Start the polling loop
		new Thread(this).start();
	}

	@Override
	public void stopped() throws Exception {
		shutdown();
		kafkaConsumer = null;
	}

	@Override
	public JsonValue apply(JsonValue... input) throws Exception {
		return null;
	}

	@Override
	public void run() {
		try {
			kafkaConsumer.subscribe(Arrays.asList(topics));

			while (true) {
				ConsumerRecords<String, String> records = kafkaConsumer.poll(Long.MAX_VALUE);
				for (ConsumerRecord<String, String> record : records) {
					try {
						eventBus.broadcast(KAFKA_EVENT_PREFIX + record.topic(), Json.from(record.value()));
					} catch (Exception e) {
						LoggerFactory.getLogger(getClass()).warn("Could not broadcast Kafka event " + record, e);
					}
				}
			}
		} catch (WakeupException e) {
			// ignore for shutdown
		} finally {
			kafkaConsumer.close();
		}
	}

	private void shutdown() {
		kafkaConsumer.wakeup();
	}

}
