package org.ibcn.limeds.templates.webmodule;

import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.webmodules.host.api.WebHelper;

@Segment
public class Bootstrapper extends WebHelper {

	@Override
	public String getWwwRoot() {
		// Configure the path for the webmodule
		return "/example";
	}

	@Override
	public String jarLocation() {
		// Configure the folder in the jar that contains the www content
		return "wwwContent";
	}

	@Override
	public String getIndexPage() {
		// Configure the index page
		return "index.html";
	}

	@Override
	public String getErrorPage() {
		// Configure the error page to display
		return "index.html";
	}

}
