package org.ibcn.limeds.basicauth;

import org.ibcn.limeds.FunctionalSegment;
import org.ibcn.limeds.annotations.Configurable;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonValue;

@Segment(factory = true, export = false)
public class Account implements FunctionalSegment {

	@Configurable
	private String username;

	@Configurable(defaultValue = "")
	private String email;

	@Configurable
	private String passwordHash;

	@Configurable(defaultValue = "")
	private String[] roles;

	@Configurable(defaultValue = "false")
	private boolean active;

	@Override
	public JsonValue apply(JsonValue... input) throws Exception {
		return Json.objectBuilder().add("username", username).add("email", email).add("passwordHash", passwordHash)
				.add("roles", Json.arrayOf(roles)).add("active", active).build();
	}

}
