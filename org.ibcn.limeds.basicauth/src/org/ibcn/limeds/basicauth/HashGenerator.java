package org.ibcn.limeds.basicauth;

import java.security.MessageDigest;

public class HashGenerator {
	
	public static String hashString(String target) throws Exception {
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		md.update(target.getBytes());

		byte byteData[] = md.digest();

		// convert the byte to hex format
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < byteData.length; i++) {
			sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
		}

		return sb.toString();
	}

}
