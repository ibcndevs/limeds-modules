package org.ibcn.limeds.basicauth;

import java.io.IOException;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collection;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ibcn.limeds.FunctionalSegment;
import org.ibcn.limeds.HttpEndpointSegment;
import org.ibcn.limeds.annotations.Configurable;
import org.ibcn.limeds.annotations.HttpMethod;
import org.ibcn.limeds.annotations.HttpOperation;
import org.ibcn.limeds.annotations.InputDoc;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Service;
import org.ibcn.limeds.annotations.Validated;
import org.ibcn.limeds.descriptors.HttpOperationDescriptor;
import org.ibcn.limeds.json.JsonPrimitive;
import org.ibcn.limeds.json.JsonValue;
import org.ibcn.limeds.security.HttpAuthProvider;
import org.ibcn.limeds.util.FilterUtil;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Segment
@Validated
public class Provider extends HttpEndpointSegment implements HttpAuthProvider {

	private static final int UUID_LENGTH = UUID.randomUUID().toString().length();
	private static final Logger LOGGER = LoggerFactory.getLogger(Provider.class);

	@Configurable(defaultValue = "LimeDS App")
	private String realm;

	@Service
	private ConfigurationAdmin configAdmin;

	@Service(filter = "(limeds.segment.id=org.ibcn.limeds.basicauth.Account.*)", required = false)
	private Collection<FunctionalSegment> accounts;

	@Override
	public Optional<JsonValue> authorizeRequest(HttpServletRequest request, HttpServletResponse response,
			HttpOperationDescriptor httpOperation) {
		String authHeader = request.getHeader("Authorization");
		if (authHeader != null && accounts != null) {
			String encodedCredentials = authHeader.substring(authHeader.indexOf(' ')).trim();
			try {
				String decodedCredentials = new String(Base64.getDecoder().decode(encodedCredentials));
				String username = decodedCredentials.split(":")[0];
				String password = decodedCredentials.split(":")[1];
				Optional<JsonValue> account = accounts.stream().filter(acc -> {
					try {
						return passwordMatch(username, password, acc.apply());
					} catch (Exception e) {
						LOGGER.warn("Error matching accounts", e);
						return false;
					}
				}).map(acc -> {
					try {
						return acc.apply();
					} catch (Exception e) {
						// Should not happen
						return null;
					}
				}).findAny();
				if (account.isPresent() && rolesMatch(account.get().get("roles").asArray().stream()
						.map(role -> role.asString()).collect(Collectors.toSet()), httpOperation)) {
					return account;
				}
			} catch (Exception e) {
				LOGGER.warn("Auth Header for request {} could not be processed.", request);
			}
		}
		// If not, write an error HTTP response...
		response.setHeader("WWW-Authenticate", "Basic realm=\"" + realm + "\"");
		try {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
					"Valid credentials are required to access this operation!");
		} catch (IOException e) {
			e.printStackTrace();
		}
		// ... and return an empty optional, indicating failure.
		return Optional.empty();
	}

	@InputDoc(label = "accountInfo", schemaRef = "org.ibcn.limeds.basicauth.Account_${sliceVersion}")
	@HttpOperation(groupId = "LimeDS Basic Auth", method = HttpMethod.POST, path = "/basic-auth/accounts")
	@Override
	public JsonValue apply(JsonValue input, HttpContext context) throws Exception {
		try {
			if (configAdmin.listConfigurations(FilterUtil.equals("username", input.getString("username"))) == null) {
				Configuration config = configAdmin.createFactoryConfiguration(Account.class.getName(), null);
				Dictionary<String, Object> props = new Hashtable<>();
				props.put("username", input.getString("username"));
				String salt = UUID.randomUUID().toString();
				props.put("passwordHash", HashGenerator.hashString(input.getString("password") + salt) + salt);
				if (input.has("roles")) {
					props.put("roles", input.get("roles").asArray().stream().map(val -> val.asString())
							.toArray(size -> new String[size]));
				}

				if (input.has("email")) {
					props.put("email", input.getString("email"));
				}

				props.put("active", false);

				props.put("$.id", Account.class.getName() + "." + input.getString("username"));

				config.update(props);
				return new JsonPrimitive("An account was created for user " + input.getString("username"));
			} else {
				return new JsonPrimitive("An account with that username already exists!");
			}
		} catch (Exception e) {
			return new JsonPrimitive("Could not create account: " + e);
		}
	}

	private boolean passwordMatch(String username, String password, JsonValue account) throws Exception {
		if (account.getBool("active") && account.getString("username").equals(username)) {
			String pwd = account.getString("passwordHash");
			String origHash = pwd.substring(0, pwd.length() - UUID_LENGTH);
			String salt = pwd.substring(pwd.length() - UUID_LENGTH);
			return origHash.equals(HashGenerator.hashString(password + salt));
		}
		return false;
	}

	private boolean rolesMatch(Set<String> roles, HttpOperationDescriptor httpOperation) {
		Set<String> requiredRoles = Arrays.stream(httpOperation.getAuthorityRequired()).collect(Collectors.toSet());
		switch (httpOperation.getAuthMode()) {
		case AUTHORIZE_CONJUNCTIVE:
			return requiredRoles.stream().allMatch(role -> roles.contains(role));
		case AUTHORIZE_DISJUNCTIVE:
			return requiredRoles.stream().anyMatch(role -> roles.contains(role));
		default:
			return true;
		}
	}

}
