#! /bin/python

import os, zipfile, json, sys

if ((len(sys.argv) < 2) or (len(sys.argv) > 3)):
  print '    Usage: generate-index.py <outputFile> [--pretty]*'
  sys.exit()

outputFile = sys.argv[1]
pretty = (len(sys.argv) == 3 and sys.argv[2] == '--pretty')

excludeList = ['./cnf', './.git', './.gradle', './gradle', './.metadata', './.recommenders', './run', './.dev-setup']
result = {}

def parseManifest(manifest):
  d = {}
  lines = []
  for line in manifest.splitlines():
    if (line.startswith(' ')):
      lines[len(lines)-1] += line.strip()
    else:
      lines.append(line)
  for line in lines:
    tokens = line.split(':')
    d[tokens[0]] = ':'.join(tokens[1:]).strip()
  return d

def readManifest(jarFile):
  zf = zipfile.ZipFile(jarFile, 'r')
  try:
    manifest = zf.read('META-INF/MANIFEST.MF')
    return parseManifest(manifest)
  finally:
    zf.close()

def prettyPrint(dict):
  print '==============='
  for key in dict:
    print key +' => '+ dict[key]

def addToResult(dict):
  print  dict['Bundle-Name']+' - '+dict['Bundle-Version']
  result[dict['Bundle-Name']+'-'+dict['Bundle-Version']] = dict

def toOutputFile():
  with open(outputFile, 'w') as outfile:
    if (pretty == True):
      json.dump(result, outfile, sort_keys=True, indent=4, separators=(',',':'))
    else:
      json.dump(result, outfile, separators=(',',':'))
  

def Main():
  for dirpath, dnames, fnames in os.walk("./"):
    if (not dirpath.startswith(tuple(excludeList))):
      for f in fnames:
        if (f.endswith('.jar') and f != 'setup.jar'):
          obj = readManifest(os.path.join(dirpath,f))
          addToResult(obj)
  toOutputFile()

Main()

