package org.ibcn.limeds.postgresql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Arrays;

import org.ibcn.limeds.FunctionalSegment;
import org.ibcn.limeds.annotations.Configurable;
import org.ibcn.limeds.annotations.InputDoc;
import org.ibcn.limeds.annotations.OutputDoc;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.Validated;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonValue;
import org.postgresql.ds.PGSimpleDataSource;

@Segment(factory = true, description = "LimeDS PostgreSQL connector", id="org.ibcn.limeds.db.PostgreSQL")
@Validated
public class Factory implements FunctionalSegment {

	@Configurable(description = "Hostname of the server running the database.")
	private String serverName;

	@Configurable(description = "Port of the server running the database.", defaultValue = "5432")
	private int portNumber;

	@Configurable(description = "Name of the target database.")
	private String databaseName;

	@Configurable(description = "User used to make database connections.")
	private String user;

	@Configurable(description = "Password used to make database connections.")
	private String password;

	@Configurable(description = "The maximum number of open database connections to allow. When more connections are requested, the caller will hang until a connection is returned to the pool.", defaultValue = "10")
	private int maxConnections;

	private PGSimpleDataSource connectionPool;

	@Override
	public void started() throws Exception {
		connectionPool = new PGSimpleDataSource();
		// connectionPool.setDataSourceName(getDescriptor().toString());
		connectionPool.setServerName(serverName);
		connectionPool.setDatabaseName(databaseName);
		connectionPool.setUser(user);
		connectionPool.setPassword(password);
		// connectionPool.setMaxConnections(maxConnections);
	}

	@Override
	public void stopped() throws Exception {
		// connectionPool.close();
		connectionPool = null;
	}

	@InputDoc(label = "sql", schema = "\"String (The sql statement to execute on the database/)\"")
	@OutputDoc(schemaRef = "org.ibcn.limeds.postgresql.DBResult_${sliceVersion}")
	@Override
	public JsonValue apply(JsonValue... input) throws Exception {
		String sql = input[0].asString();
		JsonArray parameters = Arrays.stream(input, 1, input.length).collect(Json.toArray());

		JsonObject result = new JsonObject();
		try (Connection conn = connectionPool.getConnection()) {
			PreparedStatement statement = conn.prepareStatement(sql);
			if (!parameters.isEmpty()) {
				ConvertorUtil.updateStatement(statement, parameters);
			}
			if (statement.execute()) {
				// There is a resultset
				return ConvertorUtil.toJSON(statement.getResultSet());
			}
		}

		return result;
	}

}
