package org.ibcn.limeds.postgresql;

import java.io.ByteArrayInputStream;
import java.io.StringReader;
import java.math.BigDecimal;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Date;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Base64;

import org.apache.commons.io.IOUtils;
import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonPrimitive;
import org.ibcn.limeds.json.JsonValue;

public class ConvertorUtil {

	private static final JsonValue JSON_NULL = new JsonPrimitive(null);

	public static JsonArray toJSON(ResultSet rs) throws Exception {
		ResultSetMetaData rsmd = rs.getMetaData();
		int numColumns = rsmd.getColumnCount();
		String[] columnNames = new String[numColumns];
		int[] columnTypes = new int[numColumns];

		for (int i = 0; i < columnNames.length; i++) {
			columnNames[i] = rsmd.getColumnLabel(i + 1);
			columnTypes[i] = rsmd.getColumnType(i + 1);
		}

		JsonArray result = new JsonArray();

		while (rs.next()) {
			JsonObject record = new JsonObject();

			for (int i = 0; i < columnNames.length; i++) {
				if (rs.wasNull()) {
					record.put(columnNames[i], JSON_NULL);
				} else {
					/*
					 * Add JSON entry depending on type. All binary data is
					 * converted to Base64 String. Date are represented as a
					 * long unix timestamp.
					 */
					switch (columnTypes[i]) {

					case Types.INTEGER:
						record.put(columnNames[i], rs.getInt(i + 1));
						break;
					case Types.BIGINT:
						record.put(columnNames[i], rs.getLong(i + 1));
						break;
					case Types.DECIMAL:
					case Types.NUMERIC:
						record.put(columnNames[i], rs.getBigDecimal(i + 1).doubleValue());
						break;
					case Types.FLOAT:
					case Types.REAL:
					case Types.DOUBLE:
						record.put(columnNames[i], rs.getDouble(i + 1));
						break;
					case Types.NVARCHAR:
					case Types.VARCHAR:
					case Types.LONGNVARCHAR:
					case Types.LONGVARCHAR:
						record.put(columnNames[i], rs.getString(i + 1));
						break;
					case Types.BOOLEAN:
					case Types.BIT:
						record.put(columnNames[i], rs.getBoolean(i + 1));
						break;
					case Types.BINARY:
					case Types.VARBINARY:
					case Types.LONGVARBINARY:
						record.put(columnNames[i], Base64.getEncoder().encodeToString(rs.getBytes(i + 1)));
						break;
					case Types.TINYINT:
					case Types.SMALLINT:
						record.put(columnNames[i], rs.getShort(i + 1));
						break;
					case Types.DATE:
						record.put(columnNames[i], rs.getDate(i + 1).getTime());
						break;
					case Types.TIMESTAMP:
						record.put(columnNames[i], rs.getDate(i + 1).getTime());
						break;
					case Types.BLOB:
						Blob blob = rs.getBlob(i);
						record.put(columnNames[i],
								Base64.getEncoder().encodeToString(IOUtils.toByteArray(blob.getBinaryStream())));
						blob.free();
						break;

					case Types.CLOB:
						Clob clob = rs.getClob(i);
						record.put(columnNames[i], IOUtils.toString(clob.getCharacterStream()));
						clob.free();
						break;

					default:
						throw new RuntimeException("ResultSetSerializer not yet implemented for the requested type");
					}
				}
			}

			result.add(record);
		}
		return result;
	}

	public static void updateStatement(PreparedStatement statement, JsonValue vars) throws Exception {
		ParameterMetaData md = statement.getParameterMetaData();
		for (int i = 1; i <= md.getParameterCount(); i++) {
			ConvertorUtil.setSQLVariable(statement, vars.get(i - 1).asPrimitive(), i, md.getParameterType(i));
		}
	}

	public static void setSQLVariable(PreparedStatement statement, JsonPrimitive json, int variableIndex,
			int variableType) throws Exception {
		switch (variableType) {

		case Types.INTEGER:
			statement.setInt(variableIndex, json.asInt());
			break;
		case Types.BIGINT:
			statement.setLong(variableIndex, json.asLong());
			break;
		case Types.DECIMAL:
		case Types.NUMERIC:
			statement.setBigDecimal(variableIndex, new BigDecimal(json.asDouble()));
			break;
		case Types.FLOAT:
		case Types.REAL:
		case Types.DOUBLE:
			statement.setDouble(variableIndex, json.asDouble());
			break;
		case Types.NVARCHAR:
		case Types.VARCHAR:
		case Types.LONGNVARCHAR:
		case Types.LONGVARCHAR:
			statement.setString(variableIndex, json.asString());
			break;
		case Types.BOOLEAN:
		case Types.BIT:
			statement.setBoolean(variableIndex, json.asBoolean());
			break;
		case Types.BINARY:
		case Types.VARBINARY:
		case Types.LONGVARBINARY:
			statement.setBytes(variableIndex, Base64.getDecoder().decode(json.asString()));
			break;
		case Types.TINYINT:
		case Types.SMALLINT:
			statement.setShort(variableIndex, new Integer(json.asInt()).shortValue());
			break;
		case Types.DATE:
			statement.setDate(variableIndex, new Date(json.asLong()));
			break;
		case Types.TIMESTAMP:
			statement.setTimestamp(variableIndex, new Timestamp(json.asLong()));
			break;
		case Types.BLOB:
			statement.setBlob(variableIndex, new ByteArrayInputStream(Base64.getDecoder().decode(json.asString())));
			break;

		case Types.CLOB:
			statement.setClob(variableIndex, new StringReader(json.asString()));
			break;

		default:
			throw new RuntimeException("ResultSetSerializer not yet implemented for the requested type");
		}
	}

}
