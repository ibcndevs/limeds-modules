/**
 * This package contains a H2 implementation for storage
 */
@org.osgi.annotation.versioning.Version(org.ibcn.limeds.h2.DefaultH2DB.H2_VERSION)
package org.ibcn.limeds.h2.api;