package org.ibcn.limeds.h2.api;

import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonObject;

public interface H2DB {
	/**
	 * Store the given object
	 * @param instance Name of the instance to store this object in.
	 * @param object The json object to store.
	 * @return The id of the record.
	 */
	String store(String instance, JsonObject object);
	
	/**
	 * Query for objects using query by example.
	 * @param instance Name of the instance to query.
	 * @param query The json object example to query for.
	 * @return A list of json objects that match the query.
	 */
	JsonArray query(String instance, JsonObject query);
	
	/**
	 * Removes the object that matches the given object.
	 * @param instance Name of the instance to remove this object form.
	 * @param object The object to remove.
	 * @return The object that has been removed (if it was found, null otherwise).
	 */
	JsonObject remove(String instance, JsonObject object);
	
	/**
	 * Lists all object in the given instance.
	 * @param instance Name of the instance to list.
	 * @return A list of json objects
	 */
	JsonArray list(String instance);
}
