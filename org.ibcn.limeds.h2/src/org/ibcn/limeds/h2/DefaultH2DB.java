package org.ibcn.limeds.h2;

import java.util.Optional;

import org.h2.mvstore.MVStore;
import org.ibcn.limeds.ServiceSegment;
import org.ibcn.limeds.annotations.InputDoc;
import org.ibcn.limeds.annotations.OutputDoc;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.ServiceOperation;
import org.ibcn.limeds.annotations.Validated;
import org.ibcn.limeds.h2.api.H2DB;
import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonObject;

@Segment(id="org.ibcn.limeds.db.H2",description="H2 database connector used to talk with an H2 database file.")
@Validated
public class DefaultH2DB extends ServiceSegment implements H2DB {

	public static final String H2_VERSION = "3.0.0";

	private static final String STORAGE_FILE_NAME = "limeds.mv.db";
	private MVStore store;

	@Override
	public void started() throws Exception {
		store = MVStore.open(STORAGE_FILE_NAME);
	}

	@Override
	public void stopped() throws Exception {
		store.close();
	}

	private H2DAO getDao(String instance) {
		return new H2DAO(store.openMap(instance));
	}

	@Override
	@ServiceOperation(description="Store an object in an instance of the database.")
	@InputDoc(label = "instanceName", schema = "\"String (Name of the instance in the database)\"")
	@InputDoc(label = "object", schema = "{\"@typeInfo\": \"Object (Json object to store)\"}")
	@OutputDoc(schema = "\"String (Id of the stored object record)\"")
	public String store(String instance, JsonObject object) {
		return (String) getDao(instance).save(object);
	}

	@Override
	@ServiceOperation(description="Query an instance of the database using query by example.")
	@InputDoc(label = "instanceName", schema = "\"String (Name of the instance in the database)\"")
	@InputDoc(label = "query", schema = "{\"@typeInfo\": \"Object (Json object example to query for)\"}")
	@OutputDoc(schema = "{\"@typeInfo\": \"Object (Json objects matching the query)\"}", collection = true)
	public JsonArray query(String instance, JsonObject query) {
		return getDao(instance).find(query);
	}

	@Override
	@ServiceOperation(description="Remove an object from an instance of the database.")
	@InputDoc(label = "instanceName", schema = "\"String (Name of the instance in the database)\"")
	@InputDoc(label = "object", schema = "{\"@typeInfo\": \"Object (Json object to remove)\"}")
	@OutputDoc(schema = "{\"@typeInfo\": \"Object (If found: json object that was removed. Null otherwise)\"}")
	public JsonObject remove(String instance, JsonObject object) {
		Optional<JsonObject> removedObject = getDao(instance).delete(object);
		if (removedObject.isPresent()) {
			return removedObject.get();
		}
		return null;
	}

	@Override
	@ServiceOperation(description="List all object an instance of the database.")
	@InputDoc(label = "instanceName", schema = "\"String (Name of the instance in the database)\"")
	@OutputDoc(schema = "{\"@typeInfo\": \"Object (Json objects found in this instance)\"}", collection = true)
	public JsonArray list(String instance) {
		return getDao(instance).findAll();
	}

}
