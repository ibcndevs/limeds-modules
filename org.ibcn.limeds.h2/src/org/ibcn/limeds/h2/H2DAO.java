package org.ibcn.limeds.h2;

import java.util.Optional;
import java.util.stream.Collectors;

import org.h2.mvstore.MVMap;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eaio.uuid.UUID;

public class H2DAO {

	private static final String ID_FIELD = "id";
	private static final Logger LOGGER = LoggerFactory.getLogger(H2DAO.class);
	private MVMap<String, String> map;

	public H2DAO(MVMap<String, String> map) {
		this.map = map;
	}

	public Optional<JsonObject> findById(Object id) {
		if (map.containsKey("" + id)) {
			return Optional.ofNullable(fromStorage("" + id));
		}
		return Optional.empty();
	}

	public JsonArray find(JsonObject query) {
		return map.keySet().parallelStream().map(id -> fromStorage(id)).filter(entry -> entry != null)
				.filter(entry -> matches(entry, query)).collect(Json.toArray());
	}

	private boolean matches(JsonValue entry, JsonObject query) {
		for (String key : query.keySet()) {
			if (!entry.has(key) || !query.get(key).equals(entry.get(key))) {
				return false;
			}
		}
		return true;
	}

	public JsonArray findAll() {
		try {
			return Json.from("[" + map.values().stream().collect(Collectors.joining(",")) + "]");
		} catch (Exception e) {
			LOGGER.warn("Could not deserialize table!", e);
			return null;
		}
	}

	public Object save(JsonObject object) {
		String id = null;
		if (object.has(ID_FIELD)) {
			id = object.getString(ID_FIELD);
		} else {
			id = new UUID().toString();
			object.put(ID_FIELD, id);
		}
		map.put(id, object.toString());
		return id;
	}

	public Optional<JsonObject> delete(JsonObject object) {
		String old = map.remove(object.getString(ID_FIELD));
		if (old != null) {
			try {
				return Optional.of(Json.from(old));
			} catch (Exception e) {
			}
		}
		return Optional.empty();
	}

	private JsonObject fromStorage(Object id) {
		try {
			return Json.from(map.get(id));
		} catch (Exception e) {
			LOGGER.warn("Could not deserialize object from storage", e);
			return null;
		}
	}

}
