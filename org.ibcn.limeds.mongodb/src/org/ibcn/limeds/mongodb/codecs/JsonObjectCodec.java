package org.ibcn.limeds.mongodb.codecs;

import org.bson.BsonObjectId;
import org.bson.BsonReader;
import org.bson.BsonType;
import org.bson.BsonValue;
import org.bson.BsonWriter;
import org.bson.codecs.CollectibleCodec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;
import org.bson.types.ObjectId;
import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonPrimitive;
import org.ibcn.limeds.json.JsonValue;

/**
 * A codec for working with JsonObject documents directly in Mongo
 * 
 * @author tdupont
 *
 */
public class JsonObjectCodec implements CollectibleCodec<JsonObject> {
	private static final String ID_FIELD = "_id";

	@Override
	public void encode(BsonWriter writer, JsonObject value, EncoderContext ctx) {
		writer.writeStartDocument();
		value.forEach((k, v) -> {
			writer.writeName(k);
			if (ctx.isEncodingCollectibleDocument() && ID_FIELD.equals(k)) {
				writeObjectId(writer, v.asPrimitive(), ctx);
			} else if (v instanceof JsonPrimitive) {
				writeJsonPrimitive(writer, v.asPrimitive(), ctx);
			} else if (v instanceof JsonObject) {
				encode(writer, v.asObject(), ctx);
			} else if (v instanceof JsonArray) {
				writeJsonArray(writer, v.asArray(), ctx);
			}
		});
		writer.writeEndDocument();
	}

	@Override
	public JsonObject decode(BsonReader reader, DecoderContext ctx) {
		JsonObject obj = new JsonObject();
		reader.readStartDocument();
		while (reader.readBsonType() != BsonType.END_OF_DOCUMENT) {
			String fieldName = reader.readName();
			switch (reader.getCurrentBsonType()) {
			case ARRAY:
				obj.put(fieldName, readJsonArray(reader, ctx));
			case DOCUMENT:
				obj.put(fieldName, decode(reader, ctx));
			default:
				obj.put(fieldName, readJsonPrimitive(reader, ctx));
			}
		}
		reader.readEndDocument();
		return obj;
	}

	@Override
	public Class<JsonObject> getEncoderClass() {
		return JsonObject.class;
	}

	/* Write methods - encode */

	private void writeObjectId(BsonWriter writer, JsonPrimitive value, EncoderContext ctx) {
		writer.writeObjectId(new ObjectId(value.asString()));
	}

	private void writeJsonPrimitive(BsonWriter writer, JsonPrimitive value, EncoderContext ctx) {
		Object val = value.getValue();
		if (val instanceof Boolean) {
			writer.writeBoolean(value.asBoolean());
		} else if (val instanceof Integer) {
			writer.writeInt32((Integer) val);
		} else if (val instanceof String) {
			writer.writeString((String) val);
		} else if (val instanceof Long) {
			writer.writeInt64((Long) val);
		} else if (val instanceof Double) {
			writer.writeDouble((Double) val);
		} else if (val instanceof Float) {
			writer.writeDouble(((Float) val).doubleValue());
		} else {
			throw new RuntimeException("This is not a JsonPrimitive-supported type");
		}
	}

	private void writeJsonArray(BsonWriter writer, JsonArray value, EncoderContext ctx) {
		writer.writeStartArray();
		value.forEach(v -> {
			if (v instanceof JsonPrimitive) {
				writeJsonPrimitive(writer, v.asPrimitive(), ctx);
			} else if (v instanceof JsonObject) {
				encode(writer, v.asObject(), ctx);
			} else if (v instanceof JsonArray) {
				writeJsonArray(writer, v.asArray(), ctx);
			}
		});
		writer.writeEndArray();
	}

	/* Read methods - decode */

	private JsonPrimitive readObjectId(BsonReader reader, DecoderContext ctx) {
		return new JsonPrimitive(reader.readObjectId().toHexString());
	}

	private JsonValue readJsonPrimitive(BsonReader reader, DecoderContext ctx) {
		switch (reader.getCurrentBsonType()) {
		case OBJECT_ID:
			return readObjectId(reader, ctx);
		case BOOLEAN:
			return new JsonPrimitive(reader.readBoolean());
		case INT32:
			return new JsonPrimitive(reader.readInt32());
		case INT64:
			return new JsonPrimitive(reader.readInt64());
		case DOUBLE:
			return new JsonPrimitive(reader.readDouble());
		case NULL:
			return null;
		case STRING:
		default:
			return new JsonPrimitive(reader.readString());
		}
	}

	private JsonValue readJsonArray(BsonReader reader, DecoderContext ctx) {
		JsonArray array = new JsonArray();
		reader.readStartArray();
		while (reader.readBsonType() != BsonType.END_OF_DOCUMENT) {
			switch (reader.getCurrentBsonType()) {
			case ARRAY:
				array.add(readJsonArray(reader, ctx));
			case DOCUMENT:
				array.add(decode(reader, ctx));
			default:
				array.add(readJsonPrimitive(reader, ctx));
			}
		}
		reader.readEndArray();
		return array;
	}

	@Override
	public boolean documentHasId(JsonObject obj) {
		return obj.has(ID_FIELD);
	}

	@Override
	public JsonObject generateIdIfAbsentFromDocument(JsonObject obj) {
		if (!documentHasId(obj)) {
			obj.put(ID_FIELD, ObjectId.get().toHexString());
		}
		return obj;
	}

	@Override
	public BsonValue getDocumentId(JsonObject obj) {
		if (!documentHasId(obj)) {
			throw new IllegalArgumentException("The document does not contain an _id field");
		}
		return new BsonObjectId(new ObjectId(obj.getString(ID_FIELD)));
	}
}
