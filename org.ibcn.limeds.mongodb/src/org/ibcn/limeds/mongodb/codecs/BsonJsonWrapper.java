package org.ibcn.limeds.mongodb.codecs;

import org.bson.BsonDocument;
import org.bson.BsonDocumentWrapper;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.conversions.Bson;
import org.ibcn.limeds.json.JsonObject;

/**
 * A wrapper to make sure a JsonObject is also implementing the Bson interface,
 * without changing the JsonObject classes.
 * 
 * @author tdupont
 *
 */
public class BsonJsonWrapper implements Bson {
	private JsonObject object;

	public BsonJsonWrapper(JsonObject object) {
		this.object = object;
	}

	@Override
	public <TDocument> BsonDocument toBsonDocument(Class<TDocument> documentClass, CodecRegistry registry) {
		return new BsonDocumentWrapper<JsonObject>(object, registry.get(JsonObject.class));
	}

}
