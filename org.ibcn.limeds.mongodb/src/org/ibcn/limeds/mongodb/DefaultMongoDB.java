package org.ibcn.limeds.mongodb;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.ibcn.limeds.ServiceSegment;
import org.ibcn.limeds.annotations.Configurable;
import org.ibcn.limeds.annotations.InputDoc;
import org.ibcn.limeds.annotations.OutputDoc;
import org.ibcn.limeds.annotations.Segment;
import org.ibcn.limeds.annotations.ServiceOperation;
import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonPrimitive;
import org.ibcn.limeds.json.JsonValue;
import org.ibcn.limeds.mongodb.api.MongoDB;
import org.ibcn.limeds.mongodb.codecs.JsonObjectCodec;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.ServerAddress;

@Segment(factory = true, description = "This is a Segment wrapper for the mongodb-java-driver for connecting with mongodb 3.4", id="org.ibcn.limeds.db.MongoDB")
public class DefaultMongoDB extends ServiceSegment implements MongoDB {

	public static final String MONGO_VERSION = "2.0.0";

	@Configurable(defaultValue = "localhost:27017", description = "Array of target servers that host the MongoDB cluster.")
	private String[] targetServers;

	@Configurable(description = "Name of the MongoDB database instance")
	private String databaseName;

	private MongoClient client;

	@Override
	public void started() throws Exception {
		CodecRegistry defaultCodecsRegistry = CodecRegistries.fromCodecs(new JsonObjectCodec());

		MongoClientOptions options = MongoClientOptions.builder()
				.codecRegistry(
						CodecRegistries.fromRegistries(defaultCodecsRegistry, MongoClient.getDefaultCodecRegistry()))
				.build();
		client = new MongoClient(Arrays.stream(targetServers).map(serverString -> {
			String[] parts = serverString.split(":");
			return new ServerAddress(parts[0].trim(), Integer.parseInt(parts[1].trim()));
		}).collect(Collectors.toList()), options);
	}

	@Override
	public void stopped() throws Exception {
		client.close();
		client = null;
	}

	private MongoDAO getDao(String collectionName) {
		return new MongoDAO(client.getDatabase(databaseName).getCollection(collectionName, JsonObject.class));
	}

	/* OPERATIONS */

	@Override
	@ServiceOperation(description="Store a json object in a collection of the database.")
	@InputDoc(label="collectionName", schema="\"String (Name of the collection of the database)\"")
	@InputDoc(label="object", schema="{\"@typeInfo\": \"Object (Json object to store)\"}")
	@OutputDoc(schema="\"String (String representing the unique key of this record)\"")
	public String store(String collection, JsonValue object) throws Exception {
		return (String) getDao(collection).save(object.asObject());
	}

	@Override
	@ServiceOperation
	public JsonArray query(String collection, JsonObject query) throws Exception {
		return getDao(collection).find(query);
	}

	@Override
	@ServiceOperation(description="Query a collection of the database using a Json object as query, possibly transforming the results with a projection.")
	@InputDoc(label="collectionName", schema="\"String (Name of the collection of the database)\"")
	@InputDoc(label="query", schema="{\"@typeInfo\": \"Object (Json object representing the query)\"}")
	@InputDoc(label="projection", schema="{\"@typeInfo\": \"Object (Json object representing a projection applied to the results)\"}")
	@OutputDoc(schema="{\"@typeInfo\": \"Object (Json object from the collection, possibly transformed according to a projection)\"}", collection=true)
	public JsonArray query(String collection, JsonObject query, JsonObject projection) throws Exception {
		return getDao(collection).find(query, projection);
	}

	@Override
	@ServiceOperation(description="Remove an object form a collection of the database.")
	@InputDoc(label="collectionName", schema="\"String (Name of the collection of the database)\"")
	@InputDoc(label="object", schema="{\"@typeInfo\": \"Object (Json object to remove)\"}")
	@OutputDoc(schema="{\"@typeInfo\": \"Object (Json object that is removed, null otherwise)\"}")
	public JsonValue remove(String collection, JsonObject object) throws Exception {
		Optional<JsonValue> removedObject = getDao(collection).delete(object);
		if (removedObject.isPresent()) {
			return removedObject.get();
		}
		return null;
	}

	@Override
	@ServiceOperation
	public JsonArray list(String collection) throws Exception {
		return getDao(collection).findAll();
	}

	@Override
	@ServiceOperation(description="List the records of a collection of the database, possibly transforming the results with a projection.")
	@InputDoc(label="collectionName", schema="\"String (Name of the collection of the database)\"")
	@InputDoc(label="projection", schema="{\"@typeInfo\": \"Object (Json object representing a projection applied to the results)\"}")
	@OutputDoc(schema="{\"@typeInfo\": \"Object (Json object from the collection, possibly transformed according to a projection)\"}", collection=true)
	public JsonArray list(String collection, JsonObject projection) throws Exception {
		return getDao(collection).findAll(projection);
	}

	@Override
	@ServiceOperation
	public JsonValue createIndex(String collection, JsonValue key) throws Exception {
		return new JsonPrimitive(getDao(collection).createIndex(key.asObject()));
	}

	@Override
	@ServiceOperation(description="Creates an index for a field of a collection of the database, possibly providing extra options.")
	@InputDoc(label="collectionName", schema="\"String (Name of the collection of the database)\"")
	@InputDoc(label="key", schema="{\"@tpyeInfo\": \"Object (Json object describing the key, or a string with the key name)\"}")
	@InputDoc(label="options", schema="{\"@tpyeInfo\": \"Object (Json object representing the extra options)\"}")
	public JsonValue createIndex(String collection, JsonValue key, JsonObject options) throws Exception {
		return new JsonPrimitive(getDao(collection).createIndex(key.asObject(), options));
	}

	@Override
	@ServiceOperation(description="Drop an index of a collection of the database.")
	@InputDoc(label="collectionName", schema="\"String (Name of the collection of the database)\"")
	@InputDoc(label="key", schema="{\"@tpyeInfo\": \"Object (Json object describing the key, or a string with the key name)\"}")
	public JsonValue dropIndex(String collection, JsonValue key) throws Exception {
		getDao(collection).dropIndex(key);
		return null;
	}

	@Override
	@ServiceOperation(description="List all indexes of a collection of the database.")
	@InputDoc(label="collectionName", schema="\"String (Name of the collection of the database)\"")
	@OutputDoc(schema="{\"@typeInfo\": \"Object (Json object describing an index)\"}", collection=true)
	public JsonArray listIndex(String collection) throws Exception {
		return getDao(collection).listIndexes();
	}

}
