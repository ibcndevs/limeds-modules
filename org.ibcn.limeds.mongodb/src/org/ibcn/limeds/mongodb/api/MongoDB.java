package org.ibcn.limeds.mongodb.api;

import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonValue;

public interface MongoDB {
	
	/**
	 * Store a JSON value
	 * @param collection Name of the collection.
	 * @param object The JsonValue to store
	 * @return The id String of the item stored
	 */
	String store(String collection, JsonValue object) throws Exception;
	
	/**
	 * Query the database by using a mongo JSON query object.
	 * @param collection Name of the collection.
	 * @param query The JsonObject query
	 * @return A JsonArray with results
	 */
	JsonArray query(String collection, JsonObject query) throws Exception;
	
	/**
	 * Query the database by using a mongo JSON query object.
	 * @param collection Name of the collection.
	 * @param query The JsonObject query
	 * @param project The projection defining Json object.
	 * @return A JsonArray with results
	 */
	JsonArray query(String collection, JsonObject query, JsonObject projection) throws Exception;
	
	/**
	 * Remove an object from the database.
	 * @param collection Name of the collection.
	 * @param object The object to remove
	 * @return The object that was deleted
	 */
	JsonValue remove(String collection, JsonObject object) throws Exception;
	
	/**
	 * List all Json objects in the database.
	 * @param collection Name of the collection.
	 * @return A Json array of json objects.
	 */
	JsonArray list(String collection) throws Exception;
	
	/**
	 * List all Json object in the database, transformed by the given projection.
	 * @param collection Name of the collection.
	 * @param projection The projection defining Json object.
	 * @return A Json array of json objects.
	 */
	JsonArray list(String collection, JsonObject projection) throws Exception;
	
	/**
	 * Creates an index defined by the given JsonObject.
	 * @param collection Name of the collection.
	 * @param key The JsonValue defining the keys (See mongodb docs for more info)
	 * @return
	 */
	JsonValue createIndex(String collection, JsonValue key) throws Exception;
	
	/**
	 * Creates an index defined by the given JsonObject.
	 * @param collection Name of the collection.
	 * @param key The JsonValue defining the keys (See mongodb docs for more info)
	 * @param optons JsonObject Value defining extra options
	 * @return
	 */
	JsonValue createIndex(String collection, JsonValue key, JsonObject options) throws Exception;
	
	/**
	 * Drops a certain index, defined by the key)
	 * @param collection Name of the collection.
	 * @param key The JsonValue representing the key.
	 * @return
	 */
	JsonValue dropIndex(String collection, JsonValue key) throws Exception;
	
	/**
	 * Returns a list of all current indexes.
	 * @param collection Name of the collection.
	 * @return Json array of all current keys.
	 */
	JsonArray listIndex(String collection) throws Exception;
}
