package org.ibcn.limeds.mongodb;

import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.StreamSupport;

import org.bson.BsonDocument;
import org.bson.BsonDocumentWriter;
import org.bson.codecs.CollectibleCodec;
import org.bson.codecs.EncoderContext;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.ibcn.limeds.json.Json;
import org.ibcn.limeds.json.JsonArray;
import org.ibcn.limeds.json.JsonObject;
import org.ibcn.limeds.json.JsonValue;
import org.ibcn.limeds.mongodb.codecs.BsonJsonWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoIterable;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.IndexOptions;

public class MongoDAO {

	private static final String ID_FIELD = "_id";
	private static final Logger LOGGER = LoggerFactory.getLogger(MongoDAO.class);

	private MongoCollection<JsonObject> collection;
	private CollectibleCodec<JsonObject> jsonObjectCodec;

	public MongoDAO(MongoCollection<JsonObject> collection) {
		this.collection = collection;
		this.jsonObjectCodec = (CollectibleCodec<JsonObject>) collection.getCodecRegistry().get(JsonObject.class);
	}

	/**
	 * Find by id, the id must be the toHexString id of the object.
	 * 
	 * @param id
	 *            Hex String representation of the ObjectId
	 * @return
	 */
	public Optional<JsonObject> findById(String id) {
		try {
			return Optional.ofNullable(collection.find(Filters.eq(ID_FIELD, new ObjectId(id))).first());
		} catch (Exception e) {
			LOGGER.warn("Exception while finding id " + id, e);
			return Optional.empty();
		}
	}

	public JsonArray find(JsonObject query) {
		Bson dbQuery = toQuery(query);
		try {
			return fromResultSet(collection.find(dbQuery));
		} catch (Exception e) {
			LOGGER.warn("Exception while running query " + query, e);
			return new JsonArray();
		}
	}

	public JsonArray find(JsonObject query, JsonObject projection) {
		Bson dbQuery = toQuery(query);
		try {
			return fromResultSet(collection.find(dbQuery).projection(toBSON(projection)));
		} catch (Exception e) {
			LOGGER.warn("Exception while running query " + query, e);
			return new JsonArray();
		}
	}

	/**
	 * Wraps obj in Bson, making sure it is not marked as a Collectible
	 * document. (No _id will be generated or read and it will not be converted
	 * from String to ObjectId)
	 * 
	 * @param obj
	 * @return
	 */
	private Bson toBSON(JsonObject obj) {
		return new BsonJsonWrapper(obj);
	}

	/**
	 * Writes a Bson document converting _id parameter to ObjectId, so it can be
	 * queried for equality.
	 * 
	 * @param obj
	 * @return
	 */
	private Bson toQuery(JsonObject obj) {
		BsonDocument doc = new BsonDocument();
		jsonObjectCodec.encode(new BsonDocumentWriter(doc), obj,
				EncoderContext.builder().isEncodingCollectibleDocument(true).build());
		return doc.asDocument();
	}

	public JsonArray findAll() {
		return fromResultSet(collection.find());
	}

	public JsonArray findAll(JsonObject projection) {
		return fromResultSet(collection.find().projection(toBSON(projection)));
	}

	public Object save(JsonObject object) {
		JsonObject in = jsonObjectCodec.generateIdIfAbsentFromDocument(object);
		String id = in.getString(ID_FIELD);
		ObjectId oid = new ObjectId(id);

		if (collection.find(Filters.eq(ID_FIELD, oid)).iterator().hasNext()) {
			// Object exist, so we update without the _id field
			in.remove(ID_FIELD);
			collection.findOneAndReplace(Filters.eq(ID_FIELD, oid), in);
		} else {
			// Create a new object
			collection.insertOne(in);
		}

		return id;
	}

	public Optional<JsonValue> delete(JsonObject object) {
		JsonValue doc = collection.findOneAndDelete(toQuery(object));
		if (doc != null) {
			try {
				return Optional.of(doc);
			} catch (Exception e) {
			}
		}
		return Optional.empty();
	}

	private JsonArray fromResultSet(MongoIterable<JsonObject> resultSetCursor) {
		return StreamSupport.stream(resultSetCursor.spliterator(), false).collect(Json.toArray());
	}

	public String createIndex(JsonObject keys) {
		return collection.createIndex(toBSON(keys));
	}

	public String createIndex(JsonObject keys, JsonObject options) {
		IndexOptions io = makeIndexOptions(options);
		return collection.createIndex(toBSON(keys), io);
	}

	public void dropIndex(JsonValue nameOrIndexSpec) {
		if (nameOrIndexSpec instanceof JsonObject) {
			collection.dropIndex(toBSON(nameOrIndexSpec.asObject()));
		} else {
			collection.dropIndex(nameOrIndexSpec.asString());
		}
	}

	public JsonArray listIndexes() {
		return fromResultSet(collection.listIndexes(JsonObject.class));
	}

	private IndexOptions makeIndexOptions(JsonObject options) {
		IndexOptions io = new IndexOptions();
		if (options.has("background")) {
			io.background(options.getBool("background"));
		}
		if (options.has("bits")) {
			io.bits(options.getInt("bits"));
		}
		if (options.has("bucketSize")) {
			io.bucketSize(options.getDouble("bucketSize"));
		}
		if (options.has("default_language")) {
			io.defaultLanguage(options.getString("default_language"));
		}
		if (options.has("expireAfterSeconds")) {
			io.expireAfter(options.getLong("expireAfterSeconds"), TimeUnit.SECONDS);
		}
		if (options.has("language_override")) {
			io.languageOverride(options.getString("language_override"));
		}
		if (options.has("max")) {
			io.max(options.getDouble("max"));
		}
		if (options.has("min")) {
			io.min(options.getDouble("min"));
		}
		if (options.has("name")) {
			io.name(options.getString("name"));
		}
		if (options.has("sparse")) {
			io.sparse(options.getBool("sparse"));
		}
		if (options.has("2dsphereIndexVersion")) {
			io.sphereVersion(options.getInt("2dsphereIndexVersion"));
		}
		if (options.has("storageEngine")) {
			io.storageEngine(toBSON(options.get("storageEngine").asObject()));
		}
		if (options.has("textIndexVersion")) {
			io.textVersion(options.getInt("textIndexVersion"));
		}
		if (options.has("unique")) {
			io.unique(options.getBool("unique"));
		}
		if (options.has("v")) {
			io.version(options.getInt("v"));
		}
		if (options.has("weights")) {
			io.weights(toBSON(options.get("weights").asObject()));
		}
		return io;
	}
}
